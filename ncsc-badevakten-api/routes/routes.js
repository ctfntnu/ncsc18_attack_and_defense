const express = require("express");
const serialize = require('node-serialize');
const fs = require('fs');
const jwt = require("jsonwebtoken");
const guard = require('express-jwt-permissions')({
    permissionsProperty: 'scope'
});

const StatusCode = require('../config/constants').StatusCode;
const ErrorMessage = require('../errorHandling/errorMessages');
const Scopes = require('../config/constants').Scopes;
const JwtSecret = require('../config/secrets').JwtSecret;
const Ncsc18BadevaktenClientId = require('../config/secrets').Ncsc18BadevaktenClientId;
const Ncsc18BadevaktenClientSecret = require('../config/secrets').Ncsc18BadevaktenClientSecret;
const getScope = require("../authorization/authorizationHandler").getScope;
const generateHash = require("../crypto/hashHandler").generateHash;

const TOKEN_VALID_MINUTES = 360;
const routes = express.Router();

// Check server health
routes.get('/health', function (req, res) {
    res.status(StatusCode.OK).send({ message: 'NCSC18 Badevakten Incident Report API server working...' });
});

// Badevakten Incident Report Manager login
routes.post('/login', function (req, res) {
    if  ( !req.body.username || !req.body.password ) {
        res.status(StatusCode.BadRequest).send({ error: ErrorMessage.BadRequest });
        return;
    }

    // Database connection with login credential check should have been here.
    // Instead, we use this simple credential check. 
    if ( req.body.username != Ncsc18BadevaktenClientId ) {
        res.status(StatusCode.UnprocessableEntity).send({ error: ErrorMessage.UnprocessableEntity });
        return;
    }

    if ( req.body.password != Ncsc18BadevaktenClientSecret ) {
        res.status(StatusCode.UnprocessableEntity).send({ error: ErrorMessage.UnprocessableEntity });
        return;
    }

    const exp = Math.floor(Date.now() / 1000) + (TOKEN_VALID_MINUTES * 60);
    const expDate = new Date();
    expDate.setMinutes(expDate.getMinutes() + TOKEN_VALID_MINUTES);
    const payload = {
        "iat": Date.now(),
        "exp": exp, 
        "sub": Ncsc18BadevaktenClientId,
        "scope": getScope(Ncsc18BadevaktenClientId)
    };
    const token = jwt.sign(payload, JwtSecret);

    res.status(StatusCode.OK).send({ access_token: token, exp: expDate });
});

routes.use((req, res, next) => {
    // Validate token
    var token = req.body.token || req.query.token || req.headers['authorization'];

    if ( token == null ) {
        res.sendStatus(StatusCode.Unauthorized);
        return;
    }

    /* *
     * Check that the JWT is well formed
     * Check the signature
     * Validate the standard claims
     * Check client permissions (scopes) */ 
    jwt.verify(token, JwtSecret, { ignoreExpiration: false, algorithms: ['HS256'] }, (e, decoded) => {
        if (e) {
            res.status(StatusCode.Unauthorized).send("Token not verified. Please login again.");
            return;
        } else {
            if ( !decoded.scope.includes("lifeguard:api")) {
                res.sendStatus(StatusCode.Unauthorized);
                return;
            }

            if ( decoded.sub != Ncsc18BadevaktenClientId ) {
                res.status(StatusCode.InvalidUsername).send({ error: invalidUsernameError });
                return;
            }

            req.user = decoded;

            next();
        }
    });
});

routes.get('/incidentreport/:name', guard.check([Scopes.ReadIncidentreport]), function (req, res) {
    console.log("\nGET /incidentreport/" + req.params.name);

    if ( req.params.name == null ) {
        res.status(StatusCode.BadRequest).send( {error: ErrorMessage.BadRequest});
        return;
    }

    var filename = 'vakt/' + generateHash(req.params.name) + '.txt';
    var fileContent = fs.readFileSync(filename, 'utf8');
    var deserializedContent = serialize.unserialize(fileContent);

    var responseMessage = deserializedContent;
    res.status(200).send({incidentreport: responseMessage});
});

routes.post('/incidentreport/:name', guard.check([Scopes.WriteIncidentreport]), function (req, res) {
    console.log("\nPOST /incidentreport/" + req.params.name);

    if ( req.params.name == null ) {
        res.status(StatusCode.BadRequest).send( {error: ErrorMessage.BadRequest});
        return;
    }

    var filename = 'vakt/' + generateHash(req.params.name) + '.txt';
    var fileContent = serialize.serialize(req.body.description);
    
    fs.writeFile(filename, fileContent, (err) => {  
        if (err) throw err;
    
        // success case, the file was saved
        console.log('\nIncident report saved! ', filename);
    });

    var responseMessage = 'Your incident report was successfully created.';
    res.status(200).send({incidentreport: responseMessage});
});

routes.use(function (err, req, res, next) {
    if (err.code === 'permission_denied') {
      res.status(403).send('Forbidden');
    }
});

module.exports = routes;
