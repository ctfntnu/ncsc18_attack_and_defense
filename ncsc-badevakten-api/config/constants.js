module.exports.Scopes = {
    LifeguardApi: 'lifeguard:api',
    CheckHealth: 'check:health',
    WriteIncidentreport: 'write:incidentreport',
    ReadIncidentreport: 'read:incidentreport'
};

module.exports.StatusCode = {
    OK: 200,
    BadRequest: 400,
    Unauthorized: 401,
    UnprocessableEntity: 422
}