const crypto = require('crypto');

module.exports.generateHash = function(name){
    return crypto.createHash('sha256')
    .update(name)
    .digest('hex');
}