# NCSC18 Badevakten AS API 
NCSC18 API for the company Badevakten AS to write incident reports.

## Running locally
This project require node.js, and use express.

1. npm install
2. node server.js

## Security
Secrets should ONLY be locally on your machine. 
They may be fetched from our keyvault, not our public repository.
