const Scopes = require("../config/constants").Scopes;
const Ncsc18BadevaktenClientId = require("../config/secrets").Ncsc18BadevaktenClientId;

module.exports.getScope = function (username) {
    var scope = [];

    scope.push(Scopes.LifeguardApi);

    if (username == Ncsc18BadevaktenClientId) {
        scope.push(Scopes.CheckHealth);
        scope.push(Scopes.WriteIncidentreport);
        scope.push(Scopes.ReadIncidentreport);
    }
    return scope;
}