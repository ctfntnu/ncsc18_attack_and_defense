#!/usr/bin/env python3

from ctf_gameserver.checker import BaseChecker, OK, NOTWORKING, TIMEOUT, NOTFOUND
import hashlib
import requests
from socket import timeout
from random import choice
from string import digits, ascii_uppercase

ncsc_username = "NCSC18-Badevakten-Adminuser"
ncsc_password = '959e3285fb060c22d7e75d6a928f9490391e7bfb08d1f1563f4e729850137f2e'


class BadevaktenChecker(BaseChecker):
    def __init__(self, tick, team, service, ip):
        BaseChecker.__init__(self, tick, team, service, ip)
        self._tick = tick
        self._team = team
        self._service = service
        self._ip = ip
        self._port = 5063

    def check_service(self):
        if self.check_health() is None:
            return NOTWORKING
        if self.login() is None:
            return NOTWORKING
        incident = self.genRandString(20)
        name = self.genRandString(5)
        if self.place_incident(name, incident) is None:
            return NOTWORKING
        i = self.get_incident(name)
        if i is None:
            return NOTWORKING
        if i != incident:
            return NOTWORKING
        return OK

    def place_flag(self):
        flag = self.get_flag(self._tick)
        if self.check_health() is None:
            return NOTWORKING
        if self.login() is None:
            return NOTWORKING
        incident = hashlib.sha256(flag.encode('utf-8')).hexdigest()
        name = flag
        if self.place_incident(name, incident) is None:
            return NOTWORKING
        return OK

    def check_flag(self, tick):
        flag = self.get_flag(self._tick)
        if self.check_health() is None:
            return NOTWORKING
        if self.login() is None:
            return NOTWORKING
        incident = hashlib.sha256(flag.encode('utf-8')).hexdigest()
        name = flag
        i = self.get_incident(name)
        if i is None:
            return NOTWORKING
        if incident != i:
            return NOTFOUND
        return OK

    def genRandString(self, N):
        return ''.join(choice(ascii_uppercase + digits) for _ in range(N))

    def get_url(self, path):
        return "http://{}:{}/api/{}".format(self._ip, self._port, path)

    def get(self, path):
        r = requests.get(self.get_url(path), headers={'authorization': self._token})
        return r

    def post(self, data, path):
        r = requests.post(self.get_url(path), json=data, headers={'authorization': self._token})
        return r

    def login(self):
        auth = {'username': ncsc_username, 'password': ncsc_password}
        r = requests.post(self.get_url("login"), json=auth)
        if r.status_code != 200:
            return None
        self._token = r.json()['access_token']
        return 1

    def place_incident(self, name, message):
        description = {'description': {'message': message}}
        r = self.post(description, "incidentreport/{}".format(name))
        if r.status_code != 200:
            return None
        if r.json()['incidentreport'] != "Your incident report was successfully created.":
            return None
        return 1

    def get_incident(self, name):
        r = self.get("incidentreport/{}".format(name))
        if r.status_code != 200:
            return None
        return r.json()['incidentreport']['message']

    def check_health(self):
        r = requests.get(self.get_url("health"))
        if r.status_code != 200:
            return None
        if r.text[0] == "NCSC18 Badevakten Incident Report API server working...":
            return None
        return 1
