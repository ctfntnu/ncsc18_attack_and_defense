#!/usr/bin/env python3

from ctf_gameserver.checker import BaseChecker, OK, NOTWORKING, TIMEOUT, NOTFOUND
from telnetlib import Telnet
from random import choice
from socket import timeout
import hashlib
from string import digits, ascii_uppercase


class Remote(Telnet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def recvline(self):
        try:
            return self.read_until(b'\n')
        except EOFError:
            return None
        except timeout:
            return None

    def recvlines(self, num):
        lines = []
        if num <= 0:
            return lines
        for i in range(0, num):
            line = self.recvline()
            if line is None:
                return None
            lines.append(line)
        return lines

    def recvuntil(self, text):
        try:
            return self.read_until(text.encode('utf-8'))
        except EOFError:
            return None
        except timeout:
            return None

    def sendline(self, text):
        try:
            self.write(("{}\n".format(text)).encode('utf-8'))
            return 1
        except EOFError:
            return None


class RustyChecker(BaseChecker):
    def __init__(self, tick, team, service, ip):
        BaseChecker.__init__(self, tick, team, service, ip)
        self._tick = tick
        self._team = team
        self._service = service
        self._ip = ip
        self._port = 4444
        self._sock = None

    def close_sock(self):
        self._sock.close()
        self._sock = None

    def read_notes(self, r):
        if self.menu(r, 3) is None:
            return None

        notes = []
        tmp = r.recvline()[:-1]
        if tmp is None:
            return None
        while "1. Log out" not in tmp.decode('utf-8'):
            notes.append(tmp[3:])
            tmp = r.recvline()[:-1]
            #if tmp is None:
            #    return None

        return notes

    def menu(self, r, idx):
        if r.recvuntil("> ") is None:
            return None
        if r.sendline(str(idx)) is None:
            return None
        return 1

    def login(self, r, username, password):
        if self.menu(r, 1) is None:
            return None
        if r.recvuntil("Username: ") is None:
            return None
        if r.sendline(username) is None:
            return None
        if r.recvuntil("Password: ") is None:
            return None
        if r.sendline(password) is None:
            return None
        return 1

    def log_out(self, r):
        if self.menu(r, 1) is None:
            return None
        return 1

    def leave_note(self, r, data):
        if self.menu(r, 2) is None:
            return None
        if r.recvuntil("Contents: ") is None:
            return None
        if r.sendline(data) is None:
            return None
        return 1

    def check_service(self):
        username = self.genRandString(16)
        password = self.genRandString(16)
        self._sock = self.connect()
        if self._sock is None:
            return TIMEOUT

        if self.login(self._sock, username, password) is None:
            self.close_sock()
            return NOTWORKING

        #notes = [ "yeeeeah", "haxxor", "hack the planet!" ]
        notes = [ self.genRandString(16) for i in range(5) ]
        for n in notes:
            if self.leave_note(self._sock, n) is None:
                self.close_sock()
                return NOTWORKING


        # the order might change
        tmp = self.read_notes(self._sock)
        if tmp is None or tmp == [] or len(tmp) != 5:
            self.close_sock()
            return NOTWORKING
        for n in notes:
            if n.encode('utf-8') not in tmp:
                self.close_sock()
                return NOTWORKING
        self.close_sock()
        return OK

    def exit_service(self, r):
        if self.menu(r, 4) is None:
            return None
        if r.recvline() is None:
            return None
        return 1

    def place_flag(self):
        flag = self.get_flag(self._tick)
        username = "bot"
        password = hashlib.sha1(flag.encode('utf-8')).hexdigest()
        self._sock = self.connect()
        if self._sock is None:
            return TIMEOUT
        if self.login(self._sock, username, password) is None:
            self.close_sock()
            return NOTWORKING

        note = flag
        if self.leave_note(self._sock, note) is None:
            self.close_sock()
            return NOTWORKING
        if self.exit_service(self._sock) is None:
            self.close_sock()
            return NOTWORKING
        self.close_sock()
        return OK

    def check_flag(self, tick):
        flag = self.get_flag(tick)
        username = "bot"
        password = hashlib.sha1(flag.encode('utf-8')).hexdigest()
        if self._sock is None:
            self._sock = self.connect()
            if self._sock is None:
                return TIMEOUT
        if self.login(self._sock, username, password) is None:
            self.close_sock()
            return NOTWORKING

        notes = self.read_notes(self._sock)
        if notes is None or notes == []:
            self.close_sock()
            return NOTWORKING
        if flag.encode('utf-8') not in notes:
            self.close_sock()
            return NOTWORKING
        self.close_sock()
        return OK

    def genRandString(self, N):
        return ''.join(choice(ascii_uppercase + digits) for _ in range(N))

    def connect(self):
        try:
            r = Remote(self._ip, self._port)
        except:
            return None
        return r
