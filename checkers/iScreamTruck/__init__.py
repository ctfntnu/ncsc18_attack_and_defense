#!/usr/bin/env python3

from ctf_gameserver.checker import BaseChecker, OK, NOTWORKING, TIMEOUT, NOTFOUND
from telnetlib import Telnet
import hashlib
from socket import timeout
from random import choice
from string import digits, ascii_uppercase

ice_creams = ["Kubernetis", "Lunis", "macIS", "Lollipush", "Intel pinup"]


class Remote(Telnet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def recvline(self):
        try:
            return self.read_until(b'\n')
        except EOFError:
            return None
        except timeout:
            return None

    def recvlines(self, num):
        lines = []
        if num <= 0:
            return lines
        for i in range(0, num):
            line = self.recvline()
            if line is None:
                return None
            lines.append(line)
        return lines

    def recvuntil(self, text):
        try:
            return self.read_until(text.encode('utf-8'))
        except EOFError:
            return None
        except timeout:
            return None

    def sendline(self, text):
        try:
            self.write(("{}\n".format(text)).encode('utf-8'))
            return 1
        except EOFError:
            return None


class iScreamTruckChecker(BaseChecker):
    def __init__(self, tick, team, service, ip):
        BaseChecker.__init__(self, tick, team, service, ip)
        self._tick = tick
        self._team = team
        self._service = service
        self._ip = ip
        self._port = 7070
        self._sock = None

    def close_sock(self):
        self._sock.close()
        self._sock = None

    def check_service(self):
        username = self.genRandString(5)
        password = self.genRandString(10)
        self._sock = self.connect()
        if self._sock is None:
            return TIMEOUT
        if self.login(self._sock, username, password) is None:
            self.close_sock()
            return NOTWORKING
        review = self.genRandString(20)
        if self.review_logged_in(self._sock, review) is None:
            self.close_sock()
            return NOTWORKING
        if self.logout(self._sock) is None:
            self.close_sock()
            return NOTWORKING
        reviews = self.login(self._sock, username, password)
        if reviews is None:
            self.close_sock()
            return NOTWORKING
        if reviews[0].decode('utf-8') != review:
            self.close_sock()
            return NOTWORKING
        self.menu(self._sock, 5)
        self.close_sock()
        return OK

    def place_flag(self):
        flag = self.get_flag(self._tick)
        username = "bot"
        password = hashlib.sha1(flag.encode('utf-8')).hexdigest()
        self._sock = self.connect()
        if self._sock is None:
            return TIMEOUT
        if self.login(self._sock, username, password) is None:
            self.close_sock()
            return NOTWORKING
        review = flag
        if self.review_logged_in(self._sock, review) is None:
            self.close_sock()
            return NOTWORKING
        if self.logout(self._sock) is None:
            self.close_sock()
            return NOTWORKING
        self.menu(self._sock, 5)
        self.close_sock()
        return OK

    def check_flag(self, tick):
        flag = self.get_flag(tick)
        username = "bot"
        password = hashlib.sha1(flag.encode('utf-8')).hexdigest()
        if self._sock is None:
            self._sock = self.connect()
            if self._sock is None:
                return TIMEOUT
        reviews = self.login(self._sock, username, password)
        if reviews is None or reviews == []:
            self.close_sock()
            return NOTWORKING
        if reviews[0].decode('utf-8') != flag:
            self.close_sock()
            return NOTFOUND
        if self.logout(self._sock) is None:
            self.close_sock()
            return NOTWORKING
        self.menu(self._sock, 5)
        self.close_sock()
        return OK

    def genRandString(self, N):
        return ''.join(choice(ascii_uppercase + digits) for _ in range(N))

    def connect(self):
        try:
            s = Remote(self._ip, self._port)
        except:
            return None
        return s

    def menu(self, r, idx=1):
        if r.recvuntil("> ") is None:
            return None
        if r.sendline(str(idx)) is None:
            return None
        return 1

    def choose(self, r):
        if self.menu(r, 1) is None:
            return None
        if r.recvuntil("Which one do you want?\n") is None:
            return None
        for i in ice_creams:
            tmp = r.recvline()[:-1].decode('utf-8').split(":")[-1].strip()
            if tmp != i:
                return None
        if r.recvuntil(">") is None:
            return None
        if r.sendline("1") is None:
            return None
        return 1

    def eat(self, r):
        if self.menu(r, 2) is None:
            return None
        tmp = r.recvline().decode('utf-8').split(":")
        if tmp[0] != "Kubernetis":
            return None
        return 1

    def login(self, r, username, password):
        if self.choose(r) is None:
            return None
        if self.eat(r) is None:
            return None
        if self.menu(r, 4) is None:
            return None
        if r.recvuntil("Username: ") is None:
            return None
        if r.sendline(username) is None:
            return None
        if r.recvuntil("Password: ") is None:
            return None
        if r.sendline(password) is None:
            return None
        if r.recvuntil("id: ") is None:
            return None
        h = r.recvline()[:-1]
        expected = hashlib.sha256("{}:{}".format(username, password).encode('utf-8')).hexdigest()
        if expected != h.decode('utf-8'):
            return None

        reviews = []
        while True:
            next_line = r.recvline()[:-1]
            if "1. Choose ice" in next_line.decode('utf-8'):
                return reviews
            reviews.append(next_line)
        return reviews

    def review_logged_in(self, r, data):
        if self.menu(r, 3) is None:
            return None
        if r.recvuntil("please: ") is None:
            return None
        if r.sendline(data) is None:
            return None
        return 1

    def logout(self, r):
        if self.menu(r, 4) is None:
            return None
        return 1
