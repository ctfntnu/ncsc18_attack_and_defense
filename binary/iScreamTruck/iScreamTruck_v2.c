#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <openssl/sha.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <dirent.h>
#include <fcntl.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*(a)))
#define HEXDIGEST_SIZE ((SHA256_DIGEST_LENGTH * 2) + 1)

static int logged_in;
static int running = 1;
static const char *icecream;
static char user_id[HEXDIGEST_SIZE];
static char user_path[PATH_MAX];

static void md_to_hex(unsigned char *md, size_t len, char *out)
{
	for (size_t i = 0; i < len; i++)
		snprintf(&out[i * 2], 3, "%02x", md[i]);
}

static void do_sha256(char *data, size_t len, char *out)
{
	unsigned char md[SHA256_DIGEST_LENGTH] = { 0 };

	SHA256((unsigned char *)data, len, md);
	md_to_hex(md, SHA256_DIGEST_LENGTH, out);
}

static void print_menu()
{
	puts("1. Choose ice cream");
	puts("2. Eat ice cream");
	puts("3. Write ice cream review");
	if (logged_in)
		puts("4. Log out");
	else
		puts("4. Login");
	puts("5. Exit");

	printf("> ");
}

static char *read_file(const char *path)
{
	int fd;
	char *data;
	struct stat sbuf;

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		perror("open()");
		return NULL;
	}

	if (fstat(fd, &sbuf)) {
		close(fd);
		perror("fstat()");
		return NULL;
	}

	data = calloc(1, sbuf.st_size + 1);
	if (!data) {
		close(fd);
		perror("calloc()");
		return NULL;
	}

	if (read(fd, data, sbuf.st_size) != sbuf.st_size) {
		close(fd);
		perror("read()");
		return NULL;
	}

	close(fd);

	return data;
}

static int get_num()
{
	char buf[16] = { 0 };

	if (!fgets(buf, sizeof(buf), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}

	return atoi(buf);
}

static const char *choices[] = { "Kubernetis", "Lunis", "macIS", "Lollipush", "Intel pinup" };
static void buy(void)
{
	int idx;

	puts("Which one do you want?");
	for (size_t i = 0; i < ARRAY_SIZE(choices); i++)
		printf("%zu: %s\n", i + 1, choices[i]);
	printf("> ");

	idx = get_num() - 1;
	icecream = choices[idx];

	puts("Thank you!");
}

static const char *nom[] = { "nom nom nom", "yummy", "ewwww", "terrible", "splendid", "marvelous", "awful" };
static void eat(void)
{
	unsigned int idx = rand() % ARRAY_SIZE(nom);

	if (!icecream) {
		puts("You need to buy an ice cream first!");
		return;
	}

	printf("%s: %s\n", icecream, nom[idx]);
	icecream = NULL;
}

static void write_review(char *review)
{
	char tmp[3] = { 0 };

	for (;;) {
		printf("Review please: ");
		if (read(STDIN_FILENO, review, 512) == -1) {
			perror("read()");
			exit(EXIT_FAILURE);
		}

		puts("Thank you for your valuable feedback!");
		printf("You entered: %s\n", review);
		printf("Looks good (y/n)? ");
		if (!fgets(tmp, sizeof(tmp), stdin)) {
			perror("fgets()");
			exit(EXIT_FAILURE);
		}

		if (!strcmp(tmp, "y\n"))
			break;
	}

	puts("Aaaaand it's gone!");
}

static int directory_exists(const char *path)
{
	DIR *d;

	d = opendir(path);
	if (!d)
		return 0;

	closedir(d);
	return 1;
}

static void write_review_logged_in(void)
{
	FILE *fp;
	char review[512];
	char hexdigest[HEXDIGEST_SIZE];
	char path[PATH_MAX] = { 0 };

	printf("Review please: ");
	if (!fgets(review, sizeof(review), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}

	review[strcspn(review, "\n")] = '\0';

	do_sha256(review, strlen(review), hexdigest);
	printf("digest: %s\n", hexdigest);

	snprintf(path, sizeof(path), "reviews/%s/%s", user_id, hexdigest);
	fp = fopen(path, "w+");
	if (!fp) {
		perror("fopen()");
		exit(EXIT_FAILURE);
	}

	if (fwrite(review, strlen(review), 1, fp) == 0) {
		fclose(fp);
		perror("fwrite()");
		return;
	}

	printf("wrote review to %s\n", path);

	fclose(fp);
}

static int dir_filter(const struct dirent *d)
{
	if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, ".."))
		return 0;
	return 1;
}

static void load_dirs(void)
{
	int n;
	char *name;
	char path[PATH_MAX] = { 0 };
	struct dirent **namelist;

	n = scandir(user_path, &namelist, dir_filter, alphasort);
	if (n == -1) {
		perror("scandir()");
		return;
	}

	while (n--) {
		name = namelist[n]->d_name;
	//	printf("%s\n", name);

		snprintf(path, sizeof(path), "reviews/%s/%s", user_id, name);
		char *tmp = read_file(path);
		if (tmp) {
			printf("%s\n", tmp);
			free(tmp);
		}
		free(namelist[n]);
	}

	free(namelist);
}

static void log_out(void)
{
	logged_in = 0;
	puts("You are now logged out!");
}

static void login(void)
{
	char *tmp;
	size_t len;
	char username[64] = { 0 };
	char password[64] = { 0 };

	printf("Username: ");
	if (!fgets(username, sizeof(username), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}
	username[strcspn(username, "\n")] = '\0';

	printf("Password: ");
	if (!fgets(password, sizeof(password), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}
	password[strcspn(password, "\n")] = '\0';

	len = strlen(username) + strlen(password) + 2;
	tmp = calloc(1, len);
	snprintf(tmp, len, "%s:%s", username, password);
	//printf("tmp: %s (%zu)\n", tmp, strlen(tmp));

	do_sha256(tmp, len - 1, user_id);
	printf("user id: %s\n", user_id);

	free(tmp);

	logged_in = 1;

	snprintf(user_path, sizeof(user_path), "reviews/%s", user_id);
	if (directory_exists(user_path)) {
		//puts("dir exists, loading reviews");
		load_dirs();
	} else {
		if (mkdir(user_path, 0777) == -1) {
			perror("mkdir()");
			exit(EXIT_FAILURE);
		}
	}
}

static void handler(void)
{
	int choice;
	char review[32] = { 0 };

	while (running) {
		print_menu();
		choice = get_num();

		switch (choice) {
		case 1: 
			buy();
			break;
		case 2: 
			eat();
			break;
		case 3: 
			if (logged_in) {
				write_review_logged_in();
			} else {
				write_review(review);
				memset(review, 0, sizeof(review));
			}
			break;
		case 4:
			if (logged_in)
				log_out();
			else
				login();
			break;
		case 5:
			running = 0;
			puts("See you!");
			break;
		}
	}
}

static void timeout(int sig)
{
	(void)sig;
	puts("Bye!");
	exit(EXIT_SUCCESS);
}

/*
 * setup SIGALRM
 */
static void init_stuff(void)
{
	setvbuf(stdout, NULL, _IONBF, 0);
	signal(SIGALRM, timeout);
	alarm(60);
}

int main(void)
{
	init_stuff();
	handler();

	return 0;
}
