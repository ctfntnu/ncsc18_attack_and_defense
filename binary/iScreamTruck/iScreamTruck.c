#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*(a)))

static int running = 1;
static const char *icecream;

static void print_menu()
{
	puts("1. Choose ice cream");
	puts("2. Eat ice cream");
	puts("3. Write ice cream review");
	puts("4. Exit");

	printf("> ");
}

static int get_num()
{
	char buf[16] = { 0 };

	if (!fgets(buf, sizeof(buf), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}

	return atoi(buf);
}

static const char *choices[] = { "Kubernetis", "Lunis", "macIS", "Lollipush", "Intel pinup" };
static void buy(void)
{
	int idx;

	puts("Which one do you want?");
	for (size_t i = 0; i < ARRAY_SIZE(choices); i++)
		printf("%zu: %s\n", i + 1, choices[i]);
	printf("> ");

	idx = get_num() - 1;
	icecream = choices[idx];

	//printf("lol: %p\n", icecream);
	puts("Thank you!");
}

static const char *nom[] = { "nom nom nom!", "yummy!", "ewwww", "terrible!" };
static void eat(void)
{
	unsigned int idx = rand() % ARRAY_SIZE(nom);

	if (!icecream) {
		puts("You need to buy an ice cream first!");
		return;
	}

	printf("%s: %s\n", icecream, nom[idx]);
	icecream = NULL;
}

static void write_review(char *review)
{
	for (;;) {
		printf("Review please: ");
		if (read(STDIN_FILENO, review, 512) == -1) {
			perror("read()");
			exit(EXIT_FAILURE);
		}

		puts("Thank you for your valuable feedback!");
		printf("Looks good (y/n)? %s\n", review);
		printf("> ");
		if (getchar() == (int)'y') {
			getchar();
			break;
		}
	}

	puts("Aaaaand it's gone!");
}

static void handler(void)
{
	int choice;
	char review[32];

	while (running) {
		print_menu();
		choice = get_num();

		switch (choice) {
		case 1: 
			buy();
			break;
		case 2: 
			eat();
			break;
		case 3: 
			write_review(review);
			memset(review, 0, sizeof(review));
			break;
		case 4:
			running = 0;
			puts("See you!");
			break;
		}
	}
}

static void timeout(int sig)
{
	(void)sig;
	puts("Bye!");
}

/*
 * setup SIGALRM
 */
static void init_stuff(void)
{
	setvbuf(stdout, NULL, _IONBF, 0);
	signal(SIGALRM, timeout);
	alarm(60);
}

int main(void)
{
	init_stuff();
	handler();

	return 0;
}
