import r2pipe
import sys

if len(sys.argv) != 2:
    print "Usage: {} <binary>".format(sys.argv[0])
    sys.exit()

r2p = r2pipe.open(sys.argv[1])

r2p.cmd("oo+")
print "Before patching:"
r2p.cmd("s 0x00401258")
print r2p.cmd("pd 1")

r2p.cmd("wx ba1f00")
print "After patching:"
print r2p.cmd("pd 1")

r2p.quit()
