#!/usr/bin/env python3

from ctf_gameserver.checker import BaseChecker, OK, NOTWORKING, TIMEOUT, NOTFOUND
from pwn import *
import sys
from random import choice
from string import digits, ascii_uppercase
    
class RustyChecker(BaseChecker):
    def __init__(self, tick, team, service, ip):
        BaseChecker.__init__(self, tick, team, service, ip)
        self._tick = tick
        self._team = team
        self._service = service
        self._ip = ip
        self._port = 4444
        self._remote = None

    def close_sock(self):
        self._remote.close()
        self._remote = None

    def read_notes(r):
        self.menu(r, 3)

        notes = []
        tmp = r.recvline()[:-1]
        while "1. Log out" not in tmp:
            notes.append(tmp[3:])
            tmp = r.recvline()[:-1]

        return notes

    def menu(r, idx):
        try:
            r.recvuntil("> ")
        except:
            return None
        r.sendline(str(idx))

    def log_in(r, username, password):
        self.menu(r, 1)
        r.recvuntil("Username: ")
        r.sendline(username)
        r.recvuntil("Password: ")
        r.sendline(password)

    def log_out(r):
        self.menu(r, 1)

    def leave_note(r, data):
        self.menu(r, 2)
        r.recvuntil("Contents: ")
        r.sendline(data)
        # Done!

    def check_service(self):
        username = self.genRandString(16)
        password = self.genRandString(16)
        self._remote = self.connect()
        if self._remote is None:
            return TIMEOUT

        self.log_in(self._remote, username, password)

        #notes = [ "yeeeeah", "haxxor", "hack the planet!" ]
        notes = [ self.genRandString(16) for i in range(5) ]
        for n in notes:
            self.leave_note(self._remote, n)

        # the order might change
        tmp = self.read_notes(self._remote)
        if len(tmp) != 5:
            self.close_sock()
            return NOTWORKING

        for n in notes:
            assert n in tmp
            if n not in tmp:
                self.close_sock()
                return NOTWORKING

        return OK

    def exit_service(r):
        self.menu(r, 4)
        try:
            r.recvline()
        except:
            return None

    def place_flag(self):
        flag = self.get_flag(self._tick)
        username = "bot"
        password = hashlib.sha1(flag).hexdigest()
        self._remote = self.connect()
        if self._remote is None:
            return TIMEOUT
        self.login(self._remote, username, password)

        note = flag
        self.leave_note(r, note)
        if exit_service(self._remote) == None:
            self.close_sock()
            return NOTWORKING
        return OK

    def check_flag(self, tick):
        flag = self.get_flag(tick)
        username = "bot"
        password = hashlib.sha1(flag).hexdigest()
        self._remote = self.connect()
        if self._remote is None:
            return TIMEOUT
        self.login(self._remote, username, password)

        notes = self.read_notes(self._remote)
        if flag not in notes:
            self.close_sock()
            return NOTWORKING
        return OK

    def genRandString(self, N):
        return ''.join(choice(ascii_uppercase + digits) for _ in range(N))

    def connect(self):
        try:
            r = remote(self._ip, self_port)
        except:
            return None
        if self.menu(r) is None:
            return None
        return s
