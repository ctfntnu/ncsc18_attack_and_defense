#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>
#include <time.h>

int clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	static int (*real)(clockid_t, struct timespec *);
	static int cnt = 0;

	if (!real) {
		real = dlsym(RTLD_NEXT, "clock_gettime");
	}

	cnt++;

	printf("cnt: %d\n", cnt);
	if (cnt == 2) {
		/* return some other shit */
		//printf("two yooo
		int tmp = real(clk_id, tp);
		tp->tv_sec = 1337;
		tp->tv_nsec = 0xdeadbeef;
		return tmp;
	}

	return real(clk_id, tp);
}
