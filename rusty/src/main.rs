extern crate openssl;
extern crate rand;
extern crate hex;
extern crate chrono;

use openssl::sha;
use rand::{Rng, SeedableRng, StdRng};
use std::time::{SystemTime, UNIX_EPOCH};
use std::io::{self, Read, Write};
use openssl::symm::{encrypt, decrypt, Cipher};
use std::path::Path;
use std::fs;
use std::fs::File;
use chrono::Local;

#[derive(Debug)]
struct User {
    username: String,
    hash: String,
    dir: String,
    logged_in: bool
}

fn print_menu(user: &User) {
    if user.logged_in {
        println!("1. Log out");
    } else {
        println!("1. Log in");
    }

    println!("2. Leave note");
    println!("3. Read note");
    println!("4. Exit");
    print!("> ");
    io::stdout().flush().ok().expect("Could not flush stdout");
}

fn get_num() -> Option<u32> {
    let mut input_text = String::new();
    io::stdin()
        .read_line(&mut input_text)
        .expect("failed to read from stdin");

    match input_text.trim().parse::<u32>() {
        Ok(i) => Some(i),
        Err(..) => None,
    }
}

fn do_aes_encrypt(data: &[u8], key: &[u8]) -> std::vec::Vec<u8> {
    let cipher = Cipher::aes_128_cbc();
    let iv = b"\x00\x01\x02\x03\x04\x05\x06\x07\x00\x01\x02\x03\x04\x05\x06\x07";

    let ciphertext = encrypt(cipher, key, Some(iv), data).unwrap();
    ciphertext
}

fn do_aes_decrypt(data: &[u8], key: &[u8]) -> std::vec::Vec<u8> {
    let cipher = Cipher::aes_128_cbc();
    let iv = b"\x00\x01\x02\x03\x04\x05\x06\x07\x00\x01\x02\x03\x04\x05\x06\x07";

    let plaintext = decrypt(cipher, key, Some(iv), data).unwrap();
    plaintext
}

fn do_sha256(data: String) -> String {
    let mut hasher = sha::Sha256::new();
    hasher.update(data.as_bytes());
    hex::encode(hasher.finish())
}

fn load_notes(dirname: &String, key: &[u8], decrypt: bool) -> io::Result<()> {
    let dir = Path::new(dirname);
    let mut num = 1;
    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();

        let mut f = File::open(path)?;
        let mut buffer = [0; 128];
        let num_read = f.read(&mut buffer).unwrap();

        if decrypt {
            let plain = do_aes_decrypt(&buffer[0..num_read], key);
            let plain = String::from_utf8_lossy(&plain[..]);
            println!("{}: {}", num, plain);
        } else {
            println!("{}: {}", num, hex::encode(&buffer[0..num_read]));
        }
        num += 1;
    }

    Ok(())
}

fn load_all_notes() {
    println!("loading all notes!");
    for entry in fs::read_dir("./notes").unwrap() {
        let entry = entry.unwrap();
        let path = entry.path()
            .into_os_string()
            .into_string()
            .unwrap();
        let key = path.clone();
        let key = key.split("/")
            .collect::<Vec<_>>()
            .last()
            .cloned()
            .unwrap();
        println!("{}:", path);

        if let Err(error) = load_notes(&path, key[0..16].as_bytes(), false) {
            panic!("Error loading notes: {:?}", error)
        }
    }
}

impl User {
    fn log_out(&mut self) {
        self.logged_in = false;
    }

    fn log_in(&mut self, admin: &User) {
        print!("Username: ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        let mut username = String::new();
        io::stdin()
            .read_line(&mut username)
            .expect("failed to read username");
        let username = username.trim();

        print!("Password: ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        let mut password = String::new();
        io::stdin()
            .read_line(&mut password)
            .expect("failed to read password");
        let password = password.trim();

        let joined = format!("{}:{}", username, password);
        let hash = do_sha256(joined);

        println!("user id: {}", hash);

        if username == "admin" {
            if hash == admin.hash {
                println!("Welcome admin!");
                load_all_notes();
                return;
            } else {
                println!("Invalid admin password!");
                return;
            }
        }

        self.username = username.to_string();
        self.hash = hash.clone();
        self.dir = format!("notes/{}", hash);
        self.logged_in = true;

        if Path::new(&self.dir).exists() {
            println!("Welcome back, {}!", self.username);
        } else {
            println!("Welcome, {}!", self.username);
            fs::create_dir(&self.dir).unwrap();
        }
    }

    fn read_notes(&mut self) {
        if let Err(error) = load_notes(&self.dir,
                                       self.hash[0..16].as_bytes(),
                                       true) {
            panic!("Error loading notes: {:?}", error)
        }
    }

    fn leave_note(&self) -> std::io::Result<()> {
        if !self.logged_in {
            println!("You need to log in first!");
            return Ok(());
        }

        print!("Contents: ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        let mut note = String::new();
        io::stdin()
            .read_line(&mut note)
            .expect("failed to read note contents");
        let note = note.trim().to_string();
        let hash = do_sha256(note.clone());

        let path = format!("{}/{}", &self.dir, hash);

        let mut file = File::create(path)?;
        let enc = do_aes_encrypt(note.as_bytes(), self.hash[0..16].as_bytes());
        file.write_all(&enc[..])?;

        println!("Done!");

        Ok(())
    }
}

fn main() {
    println!("Welcome to the NCSC Rusty Note service!");
    let date = Local::now();
    println!("Time: {}", date.format("%Y-%m-%d %H:%M:%S"));

    let now = SystemTime::now();
    let epoch = now.duration_since(UNIX_EPOCH)
        .expect("Time set before epoch!");
    let sec = epoch.as_secs();

    let seed: &[usize] = &[ sec as usize];
    let mut rng: StdRng = SeedableRng::from_seed(seed);
    let pw: String = rng.gen_ascii_chars().take(24).collect();

    let mut hasher = sha::Sha256::new();
    let joined = format!("{}:{}", "admin", pw);
    hasher.update(joined.as_bytes());
    let hash = hex::encode(hasher.finish());

    let admin = User {
        username: "admin".to_string(),
        dir: "".to_string(),
        hash: hash,
        logged_in: true
    };

    let mut user = User {
        username: "".to_string(),
        hash: "".to_string(),
        dir: "".to_string(),
        logged_in: false
    };

    loop {
        print_menu(&user);
        let choice = get_num();
        let choice = match choice {
            Some(i) => i,
            None => 0,
        };

        match choice {
            1 if user.logged_in => {user.log_out(); },
            1 if !user.logged_in => { user.log_in(&admin); },
            2 => {
                if let Err(error) = user.leave_note() {
                   panic!("Error leaving note: {:?}", error); 
                }
            },
            3 => { user.read_notes(); },
            4 => { println!("Bye!"); },
            _ => { println!("Invalid choice!"); },
        }

        if choice == 4 {
            break;
        }
    }
}
