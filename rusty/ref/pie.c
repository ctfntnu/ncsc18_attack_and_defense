#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <ctype.h>
#include <openssl/sha.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <openssl/aes.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*(a)))
#define HEXDIGEST_SIZE ((SHA256_DIGEST_LENGTH * 2) + 1)

struct user {
	char *username;
	char *hash; /* password hash */
	char *dir;
	int logged_in;
	struct note *list;
};

struct note {
	char *data;
	size_t size;
	struct note *next;
};

static char *aes_ofb128_encrypt(char *hash, char *data, size_t size)
{
	int num = 0;
	unsigned char iv[16] = { 0 };
	/* we only use the first 32 bytes */
	unsigned int bits = 32 * 8;
	AES_KEY key;
	char *enc;

	if (AES_set_encrypt_key((unsigned char *)hash, bits, &key)) {
		printf("Couldn't initialize AES encryption key!\n");
		exit(EXIT_FAILURE);
	}

	enc = calloc(1, size);
	AES_ofb128_encrypt((unsigned char *)data, (unsigned char *)enc,
			   size, &key, iv, &num);

	return enc;
}

static char *aes_ofb128_decrypt(char *hash, char *data, size_t size)
{
	int num = 0;
	unsigned int bits = 32 * 8;
	unsigned char iv[16] = { 0 };
	AES_KEY key;
	unsigned char *dec;

	if (AES_set_encrypt_key((unsigned char *)hash, bits, &key)) {
		printf("Couldn't initialize AES decryption key!\n");
		exit(EXIT_FAILURE);
	}

	dec = calloc(1, size + 1);
	AES_ofb128_encrypt((unsigned char *)data, dec, size, &key, iv, &num);

	return (char *)dec;
}

static struct user *admin;

static int dir_filter(const struct dirent *d)
{
	if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, ".."))
		return 0;
	return 1;
}

static char *read_file(const char *path, size_t *size)
{
	int fd;
	char *data;
	struct stat sbuf;

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		perror("open()");
		return NULL;
	}

	if (fstat(fd, &sbuf)) {
		close(fd);
		perror("fstat()");
		return NULL;
	}

	data = calloc(1, sbuf.st_size + 1);
	if (!data) {
		close(fd);
		perror("calloc()");
		return NULL;
	}

	if (read(fd, data, sbuf.st_size) != sbuf.st_size) {
		close(fd);
		perror("read()");
		return NULL;
	}

	close(fd);

	*size = sbuf.st_size;
	return data;
}

static struct note *load_notes(const char *dir)
{
	int n;
	size_t size = 0;
	struct dirent **namelist;
	struct note *ret = NULL, *cur = NULL;
	char path[PATH_MAX] = { 0 };

	n = scandir(dir, &namelist, dir_filter, alphasort);
	if (n == -1) {
		perror("scandir()");
		return NULL;
	}

	while (n--) {
		struct note *new = malloc(sizeof(struct note));
		//printf("%d: %s\n", n, namelist[n]->d_name);

		snprintf(path, sizeof(path), "%s/%s",
			dir, namelist[n]->d_name);
		new->data = read_file(path, &size);
		new->size = size;

		if (cur)
			cur->next = new;
		else
			ret = new;
		new->next = NULL;
		cur = new;

		free(namelist[n]);
	}

	free(namelist);
	
	return ret;
}

static void clear_notes(struct user *user)
{
	struct note *n, *next;

	for (n = user->list; n; n = next) {
		next = n->next;
		free(n->data);
		free(n);
	}

	user->list = NULL;
}

static void print_hex(const char *data, size_t size)
{
	for (size_t i = 0; i < size; i++)
		printf("%02x", (unsigned char)data[i]);
	putchar('\n');
}

/*
 * read all directories under notes, and load the contents
 * of all the files contained under these directories
 */
static struct note *load_all_notes(void)
{
	int n;
	char *name;
	struct dirent **namelist;
	struct note *list, *cur;
	size_t cnt;
	char path[PATH_MAX] = { 0 };

	n = scandir("./notes", &namelist, dir_filter, alphasort);
	if (n == -1) {
		perror("scandir()");
		return NULL;
	}

	while (n--) {
		name = namelist[n]->d_name;
		printf("name: %s\n", name);

		snprintf(path, sizeof(path), "notes/%s", name);
		struct note *list = load_notes(path);
		if (list == NULL) {
			printf("The user didn't leave any notes!\n");
			continue;
		}
		cnt = 0;

		printf("notes:\n");
		for (cur = list; cur; cur = cur->next) {
#if 0
			char *dec = aes_ofb128_decrypt(name, cur->data, cur->size);
			printf("%zu: %s\n", cnt++, dec);
			free(dec);
#else
			printf("%zu: ", cnt++);
			print_hex(cur->data, cur->size);
#endif
		}

		free(namelist[n]);
	}

	free(namelist);
	
	return NULL;
}

static void md_to_hex(unsigned char *md, size_t len, char *out)
{
	for (size_t i = 0; i < len; i++)
		snprintf(&out[i * 2], 3, "%02x", md[i]);
}

static void do_sha256(char *data, size_t len, char *out)
{
	unsigned char md[SHA256_DIGEST_LENGTH] = { 0 };

	SHA256((unsigned char *)data, len, md);
	md_to_hex(md, SHA256_DIGEST_LENGTH, out);
}

static char *gen_random_chars(size_t num)
{
	uint8_t *ptr;
	uint32_t tmp;
	size_t gen = 0;
	char *ret = calloc(1, num + 1);
	if (!ret) {
		perror("calloc()");
		return NULL;
	}
	
	while (gen < num) {
		tmp = rand();
		ptr = (uint8_t *)&tmp;

		for (size_t i = 0; i < 4 && gen < num; i++) {
			if (isalnum(ptr[i]))
				ret[gen++] = (char)ptr[i];
		}
	}

	return ret;
}

static void print_menu(struct user *u)
{
	if (u->logged_in) {
		puts("1. Log out");
	} else {
		puts("1. Log in");
	}
	puts("2. Leave note");
	puts("3. Read note");
	puts("4. Exit");
	printf("> ");
}

static int get_num()
{
	char buf[16] = { 0 };

	if (!fgets(buf, sizeof(buf), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}

	return atoi(buf);
}

static void logout(struct user *user)
{
	user->logged_in = 0;
	printf("%s: You are now logged out!\n", user->username);
}

static int directory_exists(const char *path)
{
	DIR *d;

	d = opendir(path);
	if (!d)
		return 0;

	closedir(d);
	return 1;
}

static void login(struct user *user)
{
	char *tmp;
	size_t len;
	char username[64] = { 0 };
	char password[64] = { 0 };

	printf("Username: ");
	if (!fgets(username, sizeof(username), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}
	username[strcspn(username, "\n")] = '\0';

	printf("Password: ");
	if (!fgets(password, sizeof(password), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}
	password[strcspn(password, "\n")] = '\0';

	len = strlen(username) + strlen(password) + 2;
	tmp = calloc(1, len);
	snprintf(tmp, len, "%s:%s", username, password);

	char *user_id = calloc(1, HEXDIGEST_SIZE);
	if (!user_id) {
		perror("calloc()");
		exit(EXIT_FAILURE);
	}
	do_sha256(tmp, len - 1, user_id);
	free(tmp);

	if (!strcmp(username, "admin")) {
		char pw_hash[HEXDIGEST_SIZE] = { 0 };
		do_sha256(password, strlen(password), pw_hash);
		if (strcmp(pw_hash, admin->hash)) {
			printf("Invalid admin password!\n");
			return;
		}


		/* successfully logged in as admin! */
		user->list = load_all_notes();
	}

	printf("user id: %s\n", user_id);
	user->logged_in = 1;
	user->hash = user_id;
	size_t dirlen = strlen("notes/") + strlen(user_id) + 1;
	user->dir = calloc(1, dirlen);
	user->username = strdup(username);

	snprintf(user->dir, dirlen, "notes/%s", user_id);

	if (directory_exists(user->dir)) {
		user->list = load_notes(user->dir);
	} else {
		if (mkdir(user->dir, 0777) == -1) {
			perror("mkdir()");
			exit(EXIT_FAILURE);
		}
	}
}

static void leave_note(struct user *user)
{
	FILE *fp;
	char note[512];
	char hexdigest[HEXDIGEST_SIZE];
	char path[PATH_MAX] = { 0 };

	if (!user->logged_in) {
		printf("You need to log in first!\n");
		return;
	}

	printf("Contents: ");
	if (!fgets(note, sizeof(note), stdin)) {
		perror("fgets()");
		exit(EXIT_FAILURE);
	}

	note[strcspn(note, "\n")] = '\0';
	do_sha256(note, strlen(note), hexdigest);
	printf("digest: %s\n", hexdigest);

	snprintf(path, sizeof(path), "%s/%s", user->dir, hexdigest);
	fp = fopen(path, "w+");
	if (!fp) {
		perror("fopen()");
		exit(EXIT_FAILURE);
	}

	char *enc = aes_ofb128_encrypt(user->hash, note, strlen(note));

	if (fwrite(enc, strlen(note), 1, fp) == 0) {
		fclose(fp);
		perror("fwrite()");
		return;
	}
	free(enc);
	fclose(fp);

	clear_notes(user);
	user->list = load_notes(user->dir);

}

static void read_note(struct user *user)
{
	size_t cnt = 0;
	struct note *n;

	if (!user->logged_in) {
		printf("You need to log in first!\n");
		return;
	}

	for (n = user->list; n; n = n->next) {
		printf("size: %zu\n", n->size);
		printf("%p\n", n);
		char *dec = aes_ofb128_decrypt(user->hash, n->data, n->size);
		printf("%zu: %s\n", cnt++, dec);
		free(dec);
	}
}

static void handler(struct user *admin)
{
	int choice;
	struct user user = { 0 };

	(void)admin;

	for (;;) {
		print_menu(&user);
		choice = get_num();
		switch (choice) {
		case 1:
			if (user.logged_in)
				logout(&user);
			else
				login(&user);
			break;
		case 2:
			leave_note(&user);
			break;
		case 3:
			read_note(&user);
			break;
		case 4:
			printf("Bye!\n");
			return;
		default:
			printf("Invalid choice %d!\n", choice);
		}
	}
}


int main(void)
{
	char *pw;
	char *user = "admin";
	char hash[HEXDIGEST_SIZE] = { 0 };
	/* TODO: print date */

	srand(time(NULL));

	pw = gen_random_chars(24);
	printf("pass: %s\n", pw);
	do_sha256(pw, 24, hash);
	printf("hash: %s\n", hash);

	admin = malloc(sizeof(struct user));
	admin->username = user;
	admin->hash = hash;
	admin->logged_in = 0;

	/* crypto test */
	char *test = "testing 123";
	char *enc = aes_ofb128_encrypt(admin->hash, test, strlen(test));
	char *dec = aes_ofb128_decrypt(admin->hash, enc, strlen(test));
	printf("dec: %s\n", dec);

	handler(admin);

	free(pw);
	
	return 0;
}
