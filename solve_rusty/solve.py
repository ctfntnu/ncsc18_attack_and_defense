from pwn import *
import subprocess
import binascii
from Crypto.Cipher import AES

def pad(m):
    return m + ("\x00" * (16 - len(m)))

admin_pw = subprocess.check_output("./target/debug/solve_rusty")[:-1]

r = remote("localhost", 4444)

def menu(idx):
    r.recvuntil("> ")
    r.sendline(str(idx))

def login(username, password):
    menu(1)
    r.recvuntil("Username: ")
    r.sendline(username)
    r.recvuntil("Password: ")
    r.sendline(password)

def read_notes():
    notes = []
    tmp = r.recvline()[:-1]
    while "1. Log in" not in tmp:
        notes.append(tmp)
        tmp = r.recvline()[:-1]

    return notes

login("admin", admin_pw)
notes = read_notes()
#print notes

keys = {}
cur = ""
for n in notes:
    if n.startswith("./"):
        cur = n
        keys[cur] = []
        continue

    if cur not in keys:
        continue

    keys[cur].append(n[3:])

#print keys
for k in keys:
    key = k.split("/")[-1][:16]
    print "key: {}".format(key)

    iv = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
    for note in keys[k]:
        print note
        c = pad(binascii.unhexlify(note))
        cipher = AES.new(key, AES.MODE_CBC, iv)
        dec = cipher.decrypt(c)
        print dec

#c = pad(binascii.unhexlify("2487078ffe865e66d734a29c038d8b31"))
##c = pad(binascii.unhexlify("303e2cac69feae7d452a084b13fceaa6"))
#cipher = AES.new(key, AES.MODE_CBC, iv)
#dec = cipher.decrypt(c)
#print dec

r.interactive()
