extern crate rand;

use std::time::{SystemTime, UNIX_EPOCH};
use rand::{Rng, SeedableRng, StdRng};

fn main() {
    let now = SystemTime::now();
    print!("{:?}", now);
    let epoch = now.duration_since(UNIX_EPOCH)
        .expect("Time set before epoch!");
    let sec = epoch.as_secs();

    let seed: &[usize] = &[sec as usize];
    let mut rng: StdRng = SeedableRng::from_seed(seed);
    let pw: String = rng.gen_ascii_chars().take(24).collect();

    println!("{}", pw);
    //println!("secs: {}", sec);
}
